/*
	App: Booking System API
	Scenario: 
		A course booking system application where a user can enroll into a course
	Type: Course Booking system (Web App)
	Description:
		A course booking system application where a user can enroll into a course
		Allows an admin to do crud operations
		Allows users to register into our database
	Features:
		User log in (user authentication)
		user registration

		Customer/Authenticated users:
		View courses
		Enroll course

		Admin users:
		Add course
		Update course
		Archive/Unarchive a course (soft delete/reactivate the course)
		View course (all courses active/inactive)
		View/manage user accounts

		All users (guests, customers, admin):
		View active courses
*/

// Data Model for the Booking system

// Two-way Embedding
/*
user{
	id - unique identifier for the document	
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin
	enrollment: [
		{
			id - document identifier
			courseId - unique identifier
			courseName, -could be optional
			isPaid,
			dateEnrolled
		}
	]
}

course {
	id - unique for the document 
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrolless: [
		{
			id - document identifier
			userId,
			isPaid,
			dateEnrolled
		}
	]
}
*/

// With Referencing
/*
	user{
		id - unique identifier for the document	
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin
	}
	course {
		id - unique for the document 
		name,
		description,
		price,
		slots,
		isActive,
		createdOn
	}
	enrollment {
		id, - unique for the document
		userId,- unique id for the user
		courseId, - unique id for the course
		courseName, - optional
		isPaid,
		dateEnrolled
	}
*/