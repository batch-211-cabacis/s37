const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

// Route for creating a course
// Refactor this route to implement user authentication for our admin when creating a course


// ACTIVITY

router.post("/",auth.verify,(req,res)=>{
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))
});


// Retrieving all the courses

router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))
});

// Retrieving all active courses

router.get("/",(req,res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController));
});


// Retrieving a specific course

router.get("/:courseId",(req,res)=>{
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController))
});


// Update a course

router.put("/:courseId",auth.verify,(req,res)=>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


// ACTIVITY

// Route for archiving a course

router.put("/:courseId/archive",auth.verify,(req,res)=>{
	courseController.archiveCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


// router.put("/:courseId/activate",auth.verify,(req,res)=>{
// 	courseController.activateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController))
// });

module.exports = router;