const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
// bcrypt is a package which allows us to hash our passwords to add layer of security for our users details

const auth = require("../auth");

// Check if the emial already exists
/*
	Steps
	1. Mongus "find" method to find duplicate emails 
	2. use the "then" method to send a response back to the front end application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(
		result=> {
			if(result.length>0){
				return true;
			}else{
				return false;
			}
		})
};

// User registration
/*
	Steps:
	1. create a new user object using the mongoose model and the information from the reqBody
	2. make sure that the password is encrypted
	3. save the new user to the data base
*/

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
};

// User authentication 
/*
	Steps;
	1. check the database if the user exists
	2. compare the password in the login form with the password stored in the database
	3. Generate/return a JSON Web Token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
};


// ACTIVITY

// module.exports.getProfile = (reqBody)=> {
// 	return User.findOne({id:reqBody.id}).then(result=>{
// 		if(result == null){
// 			return false;
// 		}
// 		result.password = "";
// 		return result;
// });
// };

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
}


// Enrol a user to a calss
/*
	Steps
	1. find the document in the data base using the userId
	2. add the courseId to the user's enrollment array
	3. update the document in MongoDB
*/
/*
	1. find the user who is enrolling and update his enrollment subdocument array. We will push the courseId in the enrollments array

	2. find the course where we re enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

	3. since we will access two collection in one action we will have to wait for the completion of the action instead of letting JavScript continue line per line.

	4. to be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding
*/

module.exports.enroll = async (data) =>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId:data.courseId});
		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});
	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		course.enrollees.push({userId:data.userId});
		return course.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});
	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		return false;
	}
};
