const Course = require("../models/Course");


// Create a new course
/*
	Steps
	1. create a new course object using the mongoose model and the information from the reqBody and the id from the header
	2. save the new user to the database
*/

// ACTIVITY



// module.exports.addCourse = (reqBody) =>{
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price,
// 			});

// 	if(reqBody.isAdmin){
// 		return newCourse.save().then((course,error)=>{
// 		if(error){
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	})
// 	}else{
// 		return false;
// 	}	
// };

// SOLUTION 2

module.exports.addCourse = (data) =>{
	console.log(data);

	if(data.isAdmin){
		let newCourse = new Course({
		name: data.course.name,
		description: data.course.description,
		price: data.course.price
			});

		return newCourse.save().then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	});
	}else{
		return false;
	}	
};



// Retrieving all courses
/*
	Steps
	1. retrieve all the courses from the database
	model.find({})
*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>{
		return result;
	})
};


// Retrieve all active courses
/*
	Steps
	1. retrieve all courses from database with the property of "isActive" to true
*/

module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result=>{
		return result;
	})
};

// Retrieve specific course
/*
	Steps:
	1. retirve the course that matches the course id procided from the URL
*/

module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	})
};

// Update a course
/*
	Steps
	1. Create a variable "updatedCourse" which will contain the info retrieved from the request body
	2. find and update the course using the course id retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

module.exports.updateCourse = (reqParams, reqBody)=>{
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
};


// ACTIVITY

// Archiving a course

module.exports.archiveCourse = (reqParams, reqBody)=>{
		let archivedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		};
		return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
};


// module.exports.activateCourse = (reqParams, reqBody)=>{
// 		let activatedCourse = {
// 			name: reqBody.name,
// 			description: reqBody.description,
// 			price: reqBody.price
// 		};
// 		return Course.findById(reqParams.courseId,activatedCourse).then((course,error)=>{
// 			if(error){
// 				return false;
// 			}else{
// 				return true;
// 			};
// 		});
// };

// SOULTION 2

// module.exports.archiveCourse = (reqParams)=>{
// 	return Course.findByIdAndUpdate(reqParams,{isActive:false}).then((course,err)=>{
// 		if(error){
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	})
// }